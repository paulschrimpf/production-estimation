---
output:
  html_document:
    self_contained: no
---

# Mixture model first stage
```{r setup, results='asis', include=FALSE}
# set up knitr and webgl
rm(list=ls())
library(knitr)
knit_hooks$set(webgl = hook_webgl)
cat('<script type="text/javascript">', 
    readLines(system.file('WebGL','CanvasMatrix.js', package = 'rgl')),
    '</script>', sep = '\n')
```

```{r load data, include=FALSE, cache=TRUE, autodep=TRUE}
df <- read.csv("data_production_function_missing2zero.csv")
df[df==0] <- NA

source("productionEstimation.R")
df$t <- df$year
df <- df[order(df$id,df$t),]
ind12 <- subset(df,industry_2==12)

## single type
# Cobb-Douglas
logF.fmla <- expression(k*bk + l*bl +
                        m*(bm)) # + bkm*k + blm*l + bmm*m)
dlogF.func <- deriv(logF.fmla,namevec="m",
                    function.arg=c("m","k","l",
                        "bm","bk","bl"))
prod.fn <- function(param,data) {
  return(dlogF.func(data$m_it,data$k_it,data$l_it,param[1],param[2],param[3])) #,param[3],param[4]))
}

gnr <- production.gnr(prod.fn, 3, lnmY_it ~ m_it + k_it + l_it,
                      y_it ~ k_it + l_it, ind12, degree=3,
                      minimize=TRUE,continuous.updating=TRUE,two.step=TRUE,
                      step1.param=c(TRUE,FALSE,FALSE))
```
## Single type results
Single type results for comparison.
```{r single, include=TRUE, cache=TRUE,autodep=TRUE}
## get epsilon and omega
bhat <- gnr$estimates
print(bhat)
eps <- gnr$epsilon(bhat)
omega <- gnr$omega(bhat)
omega.lag <- panel.lag(omega, ind12$id, ind12$t)
eps.lag <- panel.lag(eps,ind12$id,ind12$t)
var(eps)
cor(eps,eps.lag,use="complete.obs")
var(omega)
cor(omega,omega.lag,use="complete.obs")
cor(eps,omega,use="complete.obs")
```

# Mixture model
Assume there are $J$ discrete types, each occurring with probabilities
$\pi = (\pi_1, ..., \pi_J)$. The production function for type $j$ is
Cobb-Douglas with type specific coefficients,
$$ f_j(m,k,l) = \beta_m^j m + \beta_k^j k + \beta_l^j l. $$
Let $\xi_i \in (1,..., J)$ denote the type of firm $i$.

## Step 1
The share equation can then be written as
$$ \begin{aligned}
    s_{it} = & \log \frac{\partial f_{\xi_i}}{\partial m}(x;\theta) + \log E[e^\epsilon] -
    \epsilon_{it} \\\\
    = & \log \beta_m^{\xi_i} + \log E[e^\epsilon |j=\xi_i ] - \epsilon_{it}
    \end{aligned}
$$
The likelihood of $s_{i1}, ..., s_{iT}$ is then
$$
  P(\{s_{it}\}_{t=1}^T) = \sum_{j=1}^J \pi_j \prod_{t=1}^T
  f_{\epsilon}(-s_{it} + \log \beta_m^j + \log E[e^\epsilon |j ] | j).
$$
If we assume that $\epsilon | j \sim N(0, \sigma_{\epsilon,j}^2)$, then
this becomes.
$$
  P(\{s_{it}\}_{t=1}^T) = \sum_{j=1}^J \pi_j \prod_{t=1}^T
  \phi\left(\frac{-s_{it} + \log \beta_m^j + \sigma_{\epsilon,j}^2/2}{\sigma_{\epsilon,j}} \right).
$$
If we let $\mu_j = \log \beta_m^j + \sigma_{\epsilon,j}^2/2$, then we
can write the likelihood as a simple normal mixture model,
$$
  P(\{s_{it}\}_{t=1}^T) = \sum_{j=1}^J \pi_j \prod_{t=1}^T
  \phi\left(\frac{-s_{it} + \mu_j}{\sigma_{\epsilon,j}} \right).
$$
The R packate 'flexmix' (see
http://cran.r-project.org/web/packages/flexmix/vignettes/flexmix-intro.pdf)
can be used to estimate normal mixture models.
```{r step1mix, include=TRUE, cache=TRUE,autodep=TRUE}
J <- 3
library(flexmix)
norm.mix <- stepFlexmix(lnmY_it ~ 1 | id, data=ind12, k = J, nrep=5,
                        control=list(verbose=1))
summary(norm.mix)
parameters(norm.mix)
```
Alternatively, the R packate 'mixtools' (see
http://cran.r-project.org/web/packages/mixtools/vignettes/mixtools.pdf)
can be used to estimate nonparametric mixture models. This package
does not work with unbalanced panels, so only 100 of the 241 firms can
be used. 
```{r step1npmix, include=TRUE, cache=TRUE,autodep=TRUE}
library(mixtools)
library(reshape2)
share <- dcast(data=ind12, formula=id ~ t, value.var="lnmY_it")
s <- as.matrix(share[,2:ncol(share)])
share.nomiss <- s[!apply(s,1,function(x) { any(is.na(x)) }),]
np.mix <- npEM(x=share.nomiss ,mu0=J, blockid=rep(1,ncol(share.nomiss)),
               verb=TRUE, , stochastic=FALSE)
np.mix$lambdahat 
summary(np.mix)
```
Both the normal mixture and nonparametric mixture give similar
esitmates for the mixture weights and conditional means and standard
deviaitions. Even the estimated densities are faily similar.
```{r,include=FALSE, cache=TRUE,autodep=TRUE}
tmp <- plot(np.mix)
```
```{r plotmix, echo=FALSE,cache=TRUE,autodep=TRUE}
library(ggplot2)
x <- tmp$x[[1]][,1]
norm <- matrix(NA,nrow=length(x),ncol=J)
for (j in 1:J) {
  norm[,j] <-
    dnorm(x,mean=parameters(norm.mix)[1,j],sd=parameters(norm.mix)[2,j])*prior(norm.mix)[j]
}
fig.df <- melt(data.frame(x=x,np=tmp$y[[1]],norm=norm), id.vars="x")
rm(x,tmp,norm)

fig <- ggplot() +
  geom_histogram(data=ind12, aes(x=lnmY_it, y=..density..),alpha=0.2) +
  geom_line(data=fig.df,aes(x=x,y=value,colour=variable)) +
  theme_minimal()
fig
```

I'll proceed using the normal mixture estimates.

### Posteriors
The estimated posterior probabilities are generally near 0 or 1.

```{r posteriorPlot, echo=FALSE, cache=TRUE, autodep=TRUE}
fig.df <- data.frame(id=ind12$id,posterior=posterior(norm.mix))
fig.df <- unique(fig.df)
fig.df <- melt(fig.df, id.vars="id")
fig <- ggplot(data=fig.df) +
  geom_histogram(aes(x=value,fill=variable),position="dodge",binwidth=0.1) +
  theme_minimal() +
  facet_grid(~variable) + guides(fill=FALSE)
fig

fdims <-1.5*c(5.03937, 3.77953) ## figure dimensions, =128mm x 96mm = dimensions of beamer slide
pdf("posterior-3.pdf",width=fdims[1],height=fdims[2])
fig
dev.off()

```

### Estimated serial correlation in $\epsilon$
The estimated serial correlation in $\epsilon$ remains high, albeit
lower than in the single type case.
```{r, include=TRUE, cache=TRUE,autodep=TRUE}
sigma.eps <- parameters(norm.mix)[2,]
print(sigma.eps)
bm <- exp(parameters(norm.mix)[1,] - sigma.eps^2/2)
print(bm)
pi <- prior(norm.mix)
print(pi)
corr.eps <- function(mix, y=ind12$lnmY_it) {
  J <- length(prior(mix))
  s.hat <- do.call(cbind,lapply(predict(mix), function(x) { unlist(x) }))
  if (nrow(s.hat)==1) {
    s.hat <- matrix(1,nrow=nrow(ind12),ncol=1) %*% s.hat
  }
  eps <- -y %*% matrix(1,nrow=1,ncol=J) + s.hat
  eps.lag <- panel.lag(eps,i=ind12$id,t=ind12$t)
  corr.eps <- rep(NA,J)
  for (j in 1:J) {
    ej <- cbind(eps[,j],eps.lag[,j])
    inc <- rowSums(is.na(ej))==0
    w <- posterior(mix)[,j]
    corr.eps[j] <- cov.wt(ej[inc,],wt=w[inc]/sum(w[inc]),cor=TRUE)$cor[1,2]
  }
  cat("average correlation ",sum(corr.eps*prior(mix)), "\n")
  cat("component correlation ", corr.eps, "\n")
  return(corr.eps)
}
corr.eps(norm.mix)
```

We can verify that this correlation calculation is correct in a simulation.
```{r, include=TRUE, cache=TRUE, autodep=TRUE}
mu <- parameters(norm.mix)[1,]
j <- sample(1:J,prob=pi,size=max(ind12$id),replace=TRUE)
ind12$j <- j[ind12$id]
rm(j)
ind12$fake.s <- rnorm(nrow(ind12))*sigma.eps[ind12$j] + mu[ind12$j]

sim.mix <- stepFlexmix(fake.s ~ 1 | id, data=ind12, k = J,
                       nrep=5)
summary(sim.mix)
parameters(sim.mix)
corr.eps(sim.mix,y=ind12$fake.s)
```
So if the model is correctly specified, the estimated auto-correlation
of $\epsilon$ is 0.

If the number of components is misspecified, then we will estimate a
non-zero autocorrelation.
```{r, include=TRUE, cache=TRUE, autodep=TRUE}
J.wrong <- 2
sim.mix <- stepFlexmix(fake.s ~ 1 | id, data=ind12, k = J.wrong, nrep=5)
corr.eps(sim.mix,y=ind12$fake.s)
```

This suggests that perhaps 3 components is not enough. The flexmix
function uses the EM algorithm, and is only locally convergent. The following
code estimates the model 5 times for each of 1 through 8
components, and saves the estimates with the highest likelihood for
each number of components.
```{r, include=TRUE, cache=TRUE, autodep=TRUE}
manyJ <- stepFlexmix(lnmY_it ~ 1 | id, data=ind12, k = 1:8,
                     control=list(verbose=0),nrep=20)
```
We can then take the model selected by any common information criteria
and calculate the correlation of $\epsilon$
```{r, include=TRUE, autodep=TRUE}
best <- getModel(manyJ,"BIC")
summary(best)
parameters(best)
J <- ncol(parameters(best))

fig.df <- data.frame(id=ind12$id,posterior=posterior(best))
fig.df <- unique(fig.df)
fig.df <- melt(fig.df, id.vars="id")
fig <- ggplot(data=fig.df) +
  geom_histogram(aes(x=value,fill=variable),position="dodge",binwidth=0.1) +
  theme_minimal() +
  facet_grid(~variable) + guides(fill=FALSE)
fig

pdf("posterior-6.pdf",width=fdims[1],height=fdims[2])
fig
dev.off()


corr.eps(best)
```
A seven component mixture is selected, but still $\epsilon$ is highly
auto-correlated.

## Adding time effects
We can add component specific or commmon time effects to the
model. To begin with, let's make the time effects common.
```{r timeEffects, echo=TRUE, cache=TRUE, autodep=TRUE}
J <- 7
t.mix <- stepFlexmix(lnmY_it ~ 1 | id, data=ind12, k = J,
                        model=FLXMRglmfix(family="gaussian",fixed=~as.factor(year)),
                        control=list(verbose=0), nrep=5)
summary(t.mix)
parameters(t.mix)
corr.eps(t.mix)
```
Now, with type specific time effects.
```{r timeEffects2, echo=TRUE, cache=TRUE, autodep=TRUE}
J <- 7
t2.mix <- stepFlexmix(lnmY_it ~ 1 + as.factor(year) | id, data=ind12, k = J,
                        model=FLXMRglmfix(family="gaussian"),
                        control=list(verbose=0), nrep=5)
summary(t2.mix)
parameters(t2.mix)
corr.eps(t2.mix)
```
Time dummies do little to reduce the serial correlation.

## Adding capital and labor
We can also add capital and labor to the share equation. To keep the
estimation simple, I will just add them linearly without worrying
about what is the underlying produciton function.
```{r covariates, echo=TRUE, cache=TRUE, autodep=TRUE}
J <- 7
kl.mix <- stepFlexmix(lnmY_it ~ 1 + k_it + l_it | id, data=ind12, k = J,
                        model=FLXMRglmfix(family="gaussian"),
                        control=list(verbose=1), nrep=3)
summary(kl.mix)
parameters(kl.mix)
corr.eps(kl.mix)
```
