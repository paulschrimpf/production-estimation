module utils
  implicit none
  interface swap
     module procedure swap_i, swap_r, swap_rv
  end interface

  interface imaxloc
     module procedure imaxloc_r, imaxloc_i
  end interface

  interface assert_eq
     module procedure assert_eq2, assert_eq3, assert_eq4, assert_eqn
  end interface

contains

  subroutine swap_i(a,b)
    implicit none
    integer, intent(inout) :: a,b
    integer :: dum
    dum = a
    a = b
    b = dum
  end subroutine swap_i

  subroutine swap_r(a,b)
    implicit none
    real(8), intent(inout) :: a,b
    real(8) :: dum
    dum = a
    a = b
    b = dum
  end subroutine swap_r

  subroutine swap_rv(a,b)
    implicit none
    real(8), dimension(:), intent(inout) :: a,b
    real(8), dimension(size(a)) :: dum
    dum = a
    a = b
    b = dum
  end subroutine swap_rv


  subroutine nrerror(string)
    implicit none
    character(len=*), intent(in) :: string
    write (*,*) 'nrerror: ',string
    stop 'Program terminated by nrerror'
  end subroutine nrerror


  function assert_eq2(n1,n2,string)
    implicit none
    character(len=*), intent(in) :: string
    integer, intent(in) :: n1,n2
    integer :: assert_eq2
    if (n1 == n2) then
       assert_eq2 = n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       stop 'program terminated by assert_eq2'
    end if
  end function assert_eq2

  function assert_eq3(n1,n2,n3,string)
    implicit none
    character(len=*), intent(in) :: string
    integer, intent(in) :: n1,n2,n3
    integer :: assert_eq3
    if (n1 == n2 .and. n2 == n3) then
       assert_eq3 = n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       stop 'program terminated by assert_eq3'
    end if
  end function assert_eq3

  function assert_eq4(n1,n2,n3,n4,string)
    implicit none
    character(len=*), intent(in) :: string
    integer, intent(in) :: n1,n2,n3,n4
    integer :: assert_eq4
    if (n1 == n2 .and. n2 == n3 .and. n3 == n4) then
       assert_eq4 = n1
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       stop 'program terminated by assert_eq4'
    end if
  end function assert_eq4

  function assert_eqn(nn,string)
    implicit none
    character(len=*), intent(in) :: string
    integer, dimension(:), intent(in) :: nn
    integer :: assert_eqn
    if (all(nn(2:) == nn(1))) then
       assert_eqn = nn(1)
    else
       write (*,*) 'nrerror: an assert_eq failed with this tag:', string
       stop 'program terminated by assert_eqn'
    end if
  end function assert_eqn


  function mean(x)
    real(8) :: mean, x(:)
    mean = sum(x) / real(size(x),8)
  end function mean

  function vabs(v)
    real(8), dimension(:), intent(in) :: v
    real(8) :: vabs
    vabs=sqrt(dot_product(v,v))
  end function vabs

  function outerprod(a,b)
    real(8), dimension(:), intent(in) :: a,b
    real(8), dimension(size(a),size(b)) :: outerprod
    outerprod = spread(a,dim=2,ncopies=size(b)) * spread(b,dim=1,ncopies=size(a))
  end function outerprod

  subroutine unit_matrix(mat,n)
    integer, intent(in) :: n
    real(8), dimension(n,n), intent(out) :: mat
    integer :: i
    !    n=min(size(mat,1),size(mat,2))
    mat(:,:) = 0.d0
    do i=1,n
       mat(i,i) = 1.d0
    end do
  end subroutine unit_matrix



  function imaxloc_r(arr)
    real(8), dimension(:), intent(in) :: arr
    integer :: imaxloc_r
    integer, dimension(1) :: imax
    imax = maxloc(arr(:))
    imaxloc_r = imax(1)
  end function imaxloc_r

  function imaxloc_i(iarr)
    integer, dimension(:), intent(in) :: iarr
    integer, dimension(1) :: imax
    integer :: imaxloc_i
    imax = maxloc(iarr(:))
    imaxloc_i = imax(1)
  end function imaxloc_i

end module utils
