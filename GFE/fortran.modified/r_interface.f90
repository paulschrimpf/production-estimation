!< R interface
!! Just calls heuristics_complete_nocov_compute
!! @param[in] N number of individuals
!! @param[in] T time periods
!! @param[in] G number of groups
!! @param[in] Nsim number of starting values
!! @param[in] Neighbourmax neighborhood size
!! @param[in] step_max ???
!! @param[in] flag_VNS 1 to use VNS algorithj, anything else for simple iterative algorithm
!! @param[in] Y outcome data (vector of lenght NT, ordered like a (TxN) in column major order)
!! @param[in] D inclusion indicator
!!
!! @param[out] Obj_matrix        Objective value found from each starting value
!! @param[out] gi_final_matrix   Classification found from each starting value
!! @param[out] alpha_gt          Estimated mean for each time, group, and starting value
!
! This exists because the name of subroutines within modules in
! libraries as seen by R vary with platform and compiler. Therefore if
! we want to call a subroutine from R, it should not be in module.
subroutine gfe_r(N,T,G,Nsim&
     &,Neighbourmax,step_max,flag_VNS,&
     &Y,D, Obj_matrix, gi_final_matrix, alpha_gt)
  use heuristics_nocovariates
  implicit none
  integer, intent(in) :: N,T,G,Nsim,Neighbourmax,step_max,flag_VNS
  real(8), intent(in) :: Y(N*T,1), D(N,T)
  real(8), intent(out) :: Obj_matrix(Nsim), alpha_gt(T,G,Nsim)
  integer, intent(out) :: gi_final_matrix(N,Nsim)

  call heuristics_complete_nocov_compute(N,T,G,Nsim&
       &,Neighbourmax,step_max,flag_VNS,&
       &Y,D, Obj_matrix, gi_final_matrix, alpha_gt)

end subroutine gfe_r

