data {
  int<lower=1> NT;
  int<lower=1> NTmiss;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  vector[NT] m;
  real k[NT];
  real l[NT];
  vector[NT] s;
  real y[NT];
  real w[N,nmix];
  int<lower=1,upper=nmix> j;
}


parameters {
  real rho;
  #real<lower=0.0, upper=1.0> bm[nmix];
  real<lower=0.0, upper=2.0> bm;
  real<lower=0.0, upper=2.0> bk;
  real<lower=0.0, upper=2.0> bl;
  real<lower=0> sigeps;
  real<lower=0> sigeta;
  real b0[T];

  real rhoX[2,4]; # coefficient for k,l process
  cov_matrix[2] varX; # var for k,l process
  cov_matrix[3] var0; # initial variance of omega,l,k
  vector[3] mu0;
}

transformed parameters {
  real loglike_i[N];
  {
    real logE;
    vector[NT] omega;
    logE <- 0.5*sigeps^2;
    for(n in 1:NT) {
      omega[n] <- m[n] - (log(bm) + logE + b0[time[n]] +
                          bk*k[n] + bl*l[n] +
                          bm*(log(bm) + logE))/(1-bm);
    }
    for (n in 1:NT) {
      int i;
      i <- id[n];
      if (n==1 || id[n]!=id[n-1]) { # new observation
        vector[3] z0;
        z0[2] <- l[n];
        z0[3] <- k[n];
        z0[1] <- omega[n];
        loglike_i[i] <- multi_normal_log(z0,mu0,var0) - log(1-bm);
      } else {
        vector[2] zt;
        vector[2] mut;
        zt[1] <- l[n];
        zt[2] <- k[n];
        for(a in 1:2) {
          mut[a] <- rhoX[a,1] + rhoX[a,2]*omega[n-1] +
            rhoX[a,3]*l[n-1] + rhoX[a,4]*k[n-1];
        }
        loglike_i[i] <- loglike_i[i] +
          normal_log(omega[n],rho*omega[n-1],sigeta/(1-bm))
          +
          multi_normal_log(zt,mut,varX);
      }
      loglike_i[i] <- loglike_i[i] + normal_log(s[n],log(bm)+logE,sigeps);
    }
  }
}

model {

  #bk ~ normal(0.35,2); 
  #bl ~ normal(0.35,2); 
  #rho ~ normal(0,10);
  #b0 ~ normal(0,10);
  #sigeta ~ gamma(2,0.1);

  #sigeta ~ cauchy(0,5);
  #sigeps ~ cauchy(0,5);
  
  for (n in 1:N) {
    increment_log_prob(loglike_i[n]*w[n,j]);
  }
}
