data {
  int<lower=1> NT;
  #int<lower=1> degree;
  int<lower=1> NZ;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real s[NT];
  real y[NT];
  matrix[NT,NZ] Z;
}
parameters {
  #real hcoef[degree+1];
  real rho;
  real b0;
  real bk;
  real bl;
  real<lower=0> Vom;
}
transformed parameters {
  real<lower=0> bm;
  real E;
  {
  real ems[NT];
  real eps[NT];
  real tmp;
  for(n in 1:NT) {
   ems[n] <- exp(-s[n]);
  }
  bm <- 1/mean(ems);
  for(n in 1:NT) {
    eps[n] <- -s[n] + log(bm);
  }
  E <- -mean(eps);
  }
}
model {
  real omega[NT];
  real eta[NT];
  vector[NT] mi[NZ];
  vector[NZ] meanM;
  matrix[NZ,NZ] W;

  for(n in 1:NT) {
    omega[n] <- y[n] - bm*m[n] - bl*l[n] - bk*k[n] - (-s[n]+log(bm) + E) - b0;
    if (n>1 && id[n-1]==id[n] && time[n-1]==(time[n]-1)) {
      omega[n] ~ normal(rho*omega[n-1],Vom);
      eta[n] <- omega[n] - rho*omega[n-1];
      for(j in 1:NZ) {
        mi[j][n] <- eta[n]*Z[n,j];
      }
    } else {
      for(j in 1:NZ) {
        mi[j][n] <- 0;
      }        
    } 
  }
  for (j in 1:NZ) {
    meanM[j] <- mean(mi[j]);
  }
  for (j in 1:NZ) {
    for (i in j:NZ) {
      W[j,i] <- sum((mi[j]-meanM[j]).*(mi[i]-meanM[i]));
      W[i,j] <- W[j,i];
    }
  }

  ## Posterior
  increment_log_prob(multi_normal_log(sqrt(NT)*meanM, rep_vector(0,NZ), W/NT));
}
