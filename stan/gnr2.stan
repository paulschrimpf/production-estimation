data {
  int<lower=1> NT;
  int<lower=1> NTmiss;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  vector[NT] m;
  real k[NT];
  real l[NT];
  vector[NT] s;
  real y[NT];
  vector[nmix] priorPi;
  int iMiss[NTmiss];
  int tMiss[NTmiss];
}


parameters {
  real rho;
  #simplex[nmix] pi;
  real b0[T]; 
  real<lower=0.0, upper=2.0> bm;
  real<lower=0.0, upper=2.0>  bk;
  real<lower=0.0, upper=2.0>  bl;
  real<lower=0> sigeps;
  real<lower=0> sigeta;
  real<lower=0> sigom0;
  real omegaMiss[NTmiss];
}

transformed parameters {
  real logE;
  vector[N] omega[T];
  logE <- 0.5*sigeps^2;

  for(n in 1:NT) {
     int t;
     int i;
     real eps;
     t <- time[n];
     i <- id[n];
     omega[t][i] <- m[n] - (log(bm) + logE + b0[t] + bk*k[n] + bl*l[n] + bm*(log(bm) + logE))/(1-bm);
  }
  
  for(n in 1:NTmiss) {
    omega[tMiss[n]][iMiss[n]] <- omegaMiss[n];
  }
}

model {
  #bk ~ normal(0.35,2); 
  #bl ~ normal(0.35,2); 
  #rho ~ normal(0,10);
  #b0 ~ normal(0,10);
  #sigom0 ~ gamma(2,0.1);
  #sigeta ~ gamma(2,0.1);

  omega[1] ~ normal(0,sigom0/(1-bm));
  #increment_log_prob(normal_log(omega[1],0,sigom0));
  for (t in 2:T) {
    omega[t] ~ normal(rho*omega[t-1],sigeta/(1-bm));
    #increment_log_prob(normal_log(omega[t],rho*omega[t-1],sigom0));        
  }
  s ~ normal(log(bm)+logE,sigeps);
  #y ~ normal(m + log(bm) + logE, sigeps);
}
