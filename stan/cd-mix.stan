data {
  int<lower=1> NT;
  int<lower=1> NTmiss;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real s[NT];
  real y[NT];
  vector[nmix] priorPi;
  int iMiss[NTmiss];
  int tMiss[NTmiss];

}
parameters {
  real rho;
  simplex[nmix] pi;
  real b0[nmix];
  real<lower=0.01> bm[nmix];
  real bk[nmix];
  real bl[nmix];
  real omegaMiss[NTmiss,J];
  real omega0[N];
  
  real<lower=0.01> sigeps[nmix];
  real<lower=0.01> sigeta;
  real<lower=0.01> sigom0;
  
  #cov_matrix[3] Vin[nmix]; # Variance of innovates to inputs
  #cov_matrix[4] V0[nmix]; # initial variance of inputs and omega
  #vector[4] mean0[nmix]; # initial means of inputs and omega
  #matrix[3,5] rhox[nmix];   
}

transformed parameters {
  real logE[nmix];
  vector[N] omega[T,J];
  logE <- 0.5*sigeps^2;

  for(n in 1:NT) {
    int t;
    int i;
    t <- time[n];
    i <- id[n];
    for(j in 1:nmix) {
      real eps;
      eps <- -s[n] + log(bm[j]) + logE[j];
      omega[t,j][i] <- y[n] - eps - b0[j] - bm[j]*m[n] -
                               bk[j]*k[n] - bl[j]*l[n];
    }
  }
  for(n in 1:NTmiss) {
    for(j in 1:nmix) {
      omega[tMiss[n],j][iMiss[n]] <- omegaMiss[n,j];
    }
  }
}

model {
  vector[T+1] ps[nmix];
  int newObs;
  int nObs;

  pi ~ dirichlet(priorPi);
  omega0 ~ normal(0,sigom0);
  for (j in 1:nmix) {
    
  }
 
  nObs <- 0;
  newObs  <- 1;
  #for (j in 1:nmix) {
    #logE[j] <- (sigeps[j]^2/2);
    #mean0[j][4] <- 0;
  #}
  for(n in 1:NT) {
    int t;
    int i;
    t <- time[n];
    i <- id[n];
    #print(" id ", id[n], " t ", time[n]);
    newObs <- (n==1 || id[n-1]!=id[n]);
    if (newObs) {
      # starting a new person
      for (j in 1:nmix) {
        ps[j] <- rep_vector(0,T+1);
        ps[j][T+1] <- log(pi[j]);
      }
    }
    for(j in 1:nmix) {
      real eps;
      eps <- -s[n] + log(bm[j]) + logE[j];
      ps[j][t] <- normal_log(eps,0,sigeps[j]);
      omega[j,t] <- y[n] - eps - b0[j] - bm[j]*m[n] -
                            bk[j]*k[n] - bl[j]*l[n];
      if(newObs) {
        ps[j][t] <- ps[j][t] + normal_log(omega[j,t],0,sigom0);
      } else {
        real eta;
        eta <- omega[j,t] - rho*omega[j,t-1];
        ps[j][t] <- ps[j][t] + normal_log(eta,0,sigeta);
      }
    }
    #print(" after stage 1: ps=",ps);
    if (n==NT || id[n+1]!=id[n]) {
      real p[nmix];
      for (j in 1:nmix) {
        p[j] <- sum(ps[j]);
      }
      increment_log_prob(log_sum_exp(p));
      nObs <- nObs + 1;
    }
    
  }
}
