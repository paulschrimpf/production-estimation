data {
  int<lower=1> NT;
  int<lower=1> NTmiss;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real s[NT];
  real y[NT];
  vector[nmix] priorPi;
  int iMiss[NTmiss];
  int tMiss[NTmiss];
  real bm;
  real sigeps;
}
parameters {
  real rho;
  #simplex[nmix] pi;
  real b0; 
  #real<lower=0, upper=2> bm;
  real<lower=0, upper=2>  bk;
  real<lower=0, upper=2>  bl;
  #real<lower=0.01> sigeps;
  real<lower=0.01> sigeta;
  real<lower=0.01> sigom0;
  
  real omegaMiss[NTmiss];
}

transformed parameters {
  real logE;
  vector[N] omega[T];
  logE <- 0.5*sigeps^2;

  for(n in 1:NT) {
     int t;
     int i;
     real eps;
     t <- time[n];
     i <- id[n];
     eps <- -s[n] + log(bm) + logE;
     omega[t][i] <- y[n] - eps - b0 - bm*m[n] -
                                bk*k[n] - bl*l[n];
  }
  
  for(n in 1:NTmiss) {
    omega[tMiss[n]][iMiss[n]] <- omegaMiss[n];
  }
}

model {
  bm ~ normal(0.35,2); 
  bk ~ normal(0.35,2); 
  bl ~ normal(0.35,2); 
  rho ~ normal(0,10);
  b0 ~ normal(0,10);

  omega[1] ~ normal(0,sigom0);
  for (t in 2:T) {
    omega[t] ~ normal(rho*omega[t-1],sigeta);
  }
  s ~ normal(log(bm)+logE,sigeps);
  
}
