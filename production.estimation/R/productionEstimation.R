## Code for estimating production functions


#' Create lags for panel data.
#'
#' This function creates lags (or leads) of panel data variables.
#' Input data should be sorted by i, t --- e.g.
#' df <- df[order(i,t),]
#' @param x Vector or matrix to get lags of.
#' @param i unit index
#' @param t time index
#' @param lag How many time periods to lag. Can be negative if leading
#' values are desired.
#' @return Lagged copy of x.
#' @keywords panel lag
#' @export
#' @examples
#' data <- loadCFLD()
#' output.lag <- panel.lag(data$output, data$i, data$t)
panel.lag <- function(x, i, t, lag=1) {
  if (!identical(order(i,t),1:length(i))) {
    stop("inputs not sorted.")
  }
  if (is.matrix(x)) {
    return(apply(x,MARGIN=2,FUN=function(c) { panel.lag(c,i,t,lag) }))
  }
  if (length(i) != length(x) || length(i) != length(t) ) {
    stop("Inputs not same length")
  }
  if (lag>0) {
    x.lag <- x[1:(length(x)-lag)]
    x.lag[i[1:(length(i)-lag)]!=i[(1+lag):length(i)] ] <- NA
    x.lag[t[1:(length(i)-lag)]+lag!=t[(1+lag):length(i)] ] <- NA
    return(c(rep(NA,lag),x.lag))
  } else if (lag<0) {
    lag <- abs(lag)
    x.lag <- x[(1+lag):length(x)]
    x.lag[i[1:(length(i)-lag)]!=i[(1+lag):length(i)] ] <- NA
    x.lag[t[1:(length(i)-lag)]+lag!=t[(1+lag):length(i)] ] <- NA
    return(c(x.lag,rep(NA,lag)))
  } else { # lag=0
    return (x)
  }
}

################################################################################

#' Two-step control function estimates as in Olley & Pakes (1996)
#'
#' Estimates a production function using the control function method
#' Olley & Pakes (1996). Any variable can be used as the proxy for
#' productivity, including investment as in Olley & Pakes (1996) or
#' materials as in Levinsohn & Petrin (2002).
#'
#' @param prod.fmla Formula defining the production function.
#'
#' @param control.fmla Formula containing variables for the control
#' function. If control.fmla is " ~ v1 + v2 + log(v3) " and, then the
#' actual control function used will be a complete polynomial of
#' degree "degree" in those three variables
#' (i.e. poly(v1,v2,log(v3),degree)
#'
#' @param data data.frame containing the variables in the
#' formulae. Data must contain variables named "id" and "t" as firm
#' identifiers and time indices. Data must be sorted in order(id,t).
#'
#' @param predetermined Vector of strings containg names of variables
#' to use as instruments. If NA, then all variables whose coefficients
#' cannot be identified in step 1 will be used as instruments in step
#' 2. Otherwise, only the variables listed will be used as instruments
#' in step 2.
#'
#' @param degree Degree of control function.
#'
#' @param time.vary If false, then the control function will be
#' constant over time. If true, the control function will be different
#' for every period.
#'
#' @return S3 object of class cf2step.
#'
#' @keywords "Olley Pakes" "productivity" "levpet" "Levinsohn-Petrin"
#' @export
#' @examples
#' df <- loadCFLD()
#' fhat <- production.cf.2step(log(output) ~ log(labor) + log(materials)
#'                              + log(capital),
#'                              ~ invest + log(capital),
#'                              data=df, degree=2)
production.cf.2step <-
  function(prod.fmla,
           control.fmla,
           data,
           predetermined=NA,
           degree=3,
           time.vary=FALSE
           )
{
  #
  # step 1
  char.fmla <- as.character(prod.fmla)
  cf.vars <- attr(terms(control.fmla),"term.labels")
  all.var.fmla <-paste(char.fmla[2],"~",
                       paste(c(char.fmla[3],cf.vars),sep=" + ",collapse=" + "),
                       sep=" ")
  z <- model.matrix(as.formula(all.var.fmla),data=data)
  df <- data[attr(z,"dimnames")[[1]],] # drop NA's
  if (time.vary) {
    fmla <- sprintf("%s ~ as.factor(t)*poly(%s,degree=%d,raw=TRUE) + %s",
                    char.fmla[2],
                    paste(cf.vars,collapse=","),
                    degree, char.fmla[3])
  } else {
    fmla <- sprintf("%s ~ poly(%s,degree=%d,raw=TRUE) + %s",
                    char.fmla[2],
                    paste(cf.vars,collapse=","),
                    degree, char.fmla[3])
  }
  step1 <- lm(as.formula(fmla),df,na.action=na.exclude)
  # take out any identified coefficients
  x.names <- attr(terms(prod.fmla),"term.labels")
  x <- model.matrix(prod.fmla,data=df)
  b1 <- step1$coefficients[x.names]
  df$xb1 <- 0
  if (any(!is.na(b1))) {
    df$xb1 <- as.vector(x[,names(b1)[!is.na(b1)]] %*% as.matrix(b1[!is.na(b1)],ncol=1))
  }
  for (n in x.names)  if (!any(n==names(df))) {
    df <- cbind(df,x[,n])
    names(df)[ncol(df)] <- n
  }
  df$fhat <- predict(step1,newdata=df) - df$xb1

  cf.vars <- cf.vars[-which(cf.vars %in% c(x.names,"(Intercept)"))]

  # step 2
  # create data frame for step 2 minimization
  df <- df[order(df$id,df$t),]
  for (v in x.names) {
    df[,sprintf("%s.lag",v)] <- panel.lag(df[,v],df$id,df$t,1)
  }
  df[,"fhat.lag"] <- panel.lag(df[,"fhat"],df$id,df$t,1)
  df$lhs <- model.response(step1$model) - df$xb1
  # objective function = sum (sum of residuals*instruments)^2
  x2.names <- x.names[is.na(b1)]
  if (any(is.na(predetermined))) {
    warning(c("Using squared sum of step 2 residuals as objective.",
              " This is not valid if you have flexible inputs whose",
              " coefficients were not identified by step 1."))
    iv <- FALSE
  } else {
    iv <- TRUE
  }
  # drop missing observations because they mess up poly()
  betaK <- rep(0,length(x2.names))
  bk <- as.matrix(df[,x2.names]) %*% betaK
  bk.lag <-  as.matrix(df[,paste(x2.names,".lag",sep="")]) %*% betaK
  z <- model.matrix(I(lhs-bk) ~ I(fhat.lag-bk.lag),data=df)
  df.step2 <- df[attr(z,"dimnames")[[1]],]
  objective <- function(betaK, deg=degree, grad=FALSE) {
    bk <- as.matrix(df.step2[,x2.names]) %*% betaK
    bk.lag <-  as.matrix(df.step2[,paste(x2.names,".lag",sep="")]) %*% betaK
    op2 <- lm(I(lhs - bk) ~ poly(I(fhat.lag - bk.lag),degree=deg, raw=TRUE),
              data=df.step2)

    if (!iv) {
      obj <- sum(residuals(op2)^2)
    } else {
      Z <- as.matrix(df.step2[,c(predetermined,"fhat.lag"
                                 ,paste(x.names,".lag",sep=""))])
      obj <- sum( (residuals(op2) %*% Z)^2 )
    }
    if (grad) {
      library(polynom)
      p <- polynomial(coefficients(op2))
      dp <- deriv(p)
      if (!iv) {
        g <- 2*( -residuals(op2) %*% as.matrix(df.step2[,x2.names]) +
                t((residuals(op2)*predict(dp,df.step2$fhat.lag-bk.lag))) %*%
                as.matrix(df.step2[,paste(x2.names,".lag",sep="")]) )
      } else {
        ## envelope theorem does not apply, since first regression
        ## minimizes sum of residuals squared, but objective function
        ## involves instruments. As a result derivative is
        ## complicated, so haven't worked it out yet.
        g <- NA
        warning("Gradient not available with instruments.")
        #g <- 2*residuals(op2) %*% Z %*% t(Z) %*%
        #  (-as.matri(xdf.step2[,x2.names]) +
        #   predict(dp,df.step2$fhat.lag-bk.lag)*
        #   as.matrix(df.step2[,paste(x2.names,".lag",sep="")]) )
      }
    } else {
      g <- NA
    }
    return(list(objective=obj, gradient=g))
  }

  ## minimize objective
  require(nloptr)
  x0 <- rep(0.2,length(x2.names))
  if (!iv) {
    opt.out <- nloptr(x0, function(x) { objective(x,degree,TRUE) },
                      opts=list(print_level=3,
                          check_derivatives=TRUE,
                          algorithm="NLOPT_LD_MMA",
                          maxeval=1000,
                          xtol_rel=1e-6))
  } else {
    opt.out <- nloptr(x0, function(x) { objective(x,degree,FALSE)$obj },
                      opts=list(print_level=3,
                          algorithm="NLOPT_LN_BOBYQA",
                          maxeval=5000,
                          xtol_rel=1e-6))
  }
  bk <- opt.out$solution
  names(bk) <- x2.names
  estimates <- opt.out$solution

  result <- list(step1=step1,objective=objective,b1=b1,
                 bk=bk, opt.out=opt.out, prod.fmla=prod.fmla,
                 control.fmla=control.fmla, degree=degree)
  class(result) <- "cf2step"
  return(result)
}

#' @export
print.cf2step <- function(x, ...) {
  cat("\nProduction function estimated by control function in two steps\n")
  coef <- x$b1
  for(v in names(x$bk)) {
    coef[v] <- x$bk[v]
  }
  cat("\nCoefficients:\n")
  print(coef, ...)

  cat("\nControl function degree=",x$degree,", formula:\n")
  print(x$control.fmla, ...)
  cat("\n\n")
  return(invisible(x))
}


################################################################################
## 1-step control function estimates as in Wooldridge (2009)
## Note: minimization of this objective function is unreliable. There
## may be an error in this code.
production.cf.wooldridge <- function
(prod.fmla, control.fmla, data,
 degree=3,
 predetermined=NA,
 time.vary=FALSE,
 minimize=TRUE,
 random.walk=FALSE ## Assume productivity follows random walk with
                   ## drift? (makes moment conditions linear in
                   ## parameters, so estimation easy) -- not yet
                   ## implemented

 ) {
  char.fmla <- as.character(prod.fmla)
  cf.vars <- attr(terms(control.fmla),"term.labels")
  if (time.vary) {
    fmla <- sprintf("%s ~ as.factor(t)*poly(%s,degree=%d,raw=TRUE) + %s",
                    char.fmla[2],
                    paste(cf.vars,collapse=","),
                    degree, char.fmla[3])
  } else {
    fmla <- sprintf("%s ~ poly(%s,degree=%d,raw=TRUE) + %s",
                    char.fmla[2],
                    paste(cf.vars,collapse=","),
                    degree, char.fmla[3])
  }
  df <- subset(data, !apply(data[,cf.vars], 1, function(x)
                            any(is.na(x))))
  df <- df[order(df$id,df$t),]
  mf <- model.frame(fmla, df)
  x1 <- model.matrix(fmla,mf)
  y <- model.response(mf,"numeric")
  df <- cbind(df[,c("id","t")],x1,y)

  x.names <- attr(terms(prod.fmla),"term.labels")
  cf.vars <- colnames(x1)
  cf.vars <- cf.vars[-which(cf.vars %in% c(x.names,"(Intercept)"))]
  for (v in x.names) {
    df[,sprintf("%s.lag",v)] <- panel.lag(df[,v],df$id,df$t,1)
  }
  for (v in cf.vars) {
    df[,sprintf("%s.lag",v)] <- panel.lag(df[,v],df$id,df$t,1)
  }
  # drop missing observations because they mess up poly()
  df <- subset(df, !apply(df, 1, function(x)
                          any(is.na(x))))

  # instruments
  z1 <- as.matrix(df[,c(x.names,cf.vars)])
  if (!any(is.na(predetermined))) {
    z2 <- as.matrix(df[,c(predetermined, paste(c(x.names,cf.vars),".lag",sep=""))])
  } else {
    z2 <- as.matrix(df[,paste(c(x.names,cf.vars),".lag",sep="")])
  }

  # moments
  X <- as.matrix(df[,x.names])
  C <- as.matrix(df[,cf.vars])
  C.lag <- as.matrix(df[,paste(cf.vars,".lag",sep="")])
  moments <- function(intercept1,beta,lambda,rho, grad=FALSE) {
    xb <- X %*% beta
    cf <- C %*% lambda
    cf.lag <- C.lag %*% lambda
    r1 <- df$y - intercept1 - xb - cf
    r2 <- df$y - xb - predict(polynomial(rho),cf.lag)
    if (grad) {
      dr1 <-
        matrix(NA,nrow=length(r1),ncol=(1+length(beta)+length(lambda)+length(rho)))
      dr2 <- dr1
      # intercept1
      dr1[,1] <- -1
      dr2[,1] <- 0
      j <- 1
      # beta
      dr1[,(j+1):(j+length(beta))] <- -X
      dr2[,(j+1):(j+length(beta))] <- -X
      j <- j+length(beta)
      # lambda
      dr1[,(j+1):(j+length(lambda))] <- -C
      dg <- predict(deriv(polynomial(rho)),cf.lag)
      dr2[,(j+1):(j+length(lambda))] <- -as.vector(dg)*C.lag
      j <- j+length(lambda)
      # rho
      dr1[,(j+1):(j+length(rho))] <- 0
      dr2[,j+1] <- -1
      for (p in 2:length(rho)) {
        dr2[,j+p] <- cf.lag*dr2[,j+(p-1)]
      }
    } else {
      dr1 <- NA
      dr2 <- NA
    }
    return(list(r1=r1,r2=r2,dr1=dr1,dr2=dr2))
  }

  N <- nrow(df)

  gmm.objective <- function(theta, W=diag(rep(1,ncol(z1)+ncol(z2))),
                            grad=FALSE, deg=degree) {
    intercept <- theta[1]
    j <- 1
    beta <- theta[(j+1):(j+length(x.names))]
    j <- j+length(x.names)
    lambda <- theta[(j+1):(j+length(cf.vars))]
    j <- j+length(cf.vars)
    rho <- theta[(j+1):(j+deg)]

    m <- moments(intercept, beta, lambda, rho, grad)


    rz <- matrix(c(t(m$r1) %*% z1, t(m$r2) %*% z2), nrow=1)
    obj <- rz %*% W %*% t(rz) / N

    g <- NA
    if (grad) {
      drz <- cbind(t(m$dr1) %*% z1, t(m$dr2) %*% z2)
      g <- 2*drz %*% W %*% t(rz) / N
    }
    return(list(objective=obj,gradient=g))
  }

  ## ## minimize objective
  if (minimize) {
    ## initial value from 2 step
    twostep <- production.cf.2step(prod.fmla, control.fmla, data,
                                   degree, predetermined, time.vary)
    require(nloptr)
    x0 <- rep(1,ncol(x1)+degree)
    opt.out <- nloptr(x0, function(x) { gmm.objective(x,deg=degree,grad=TRUE) },
                      opts=list(print_level=3,
                          check_derivatives=TRUE,
                          algorithm="NLOPT_LD_MMA",
                          maxeval=100*length(x0),
                          xtol_rel=1e-6))
    estimates <- opt.out$solution
    names(estimates) <- c("intercept1",
                          x.names, cf.vars, sprintf("rho%d",1:degree))
  } else {
    opt.out <- NA
    estimates <- NA
  }
  return(list(estimates=estimates, opt.out=opt.out, objective=gmm.objective))
}

################################################################################
## Gandhi, Navarro, & Rivers estimator

# scel.R from http://statweb.stanford.edu/~owen/empirical/scel.R
if (!file.exists("scel.R")) {
  download.file(url="http://statweb.stanford.edu/~owen/empirical/scel.R",
                destfile="scel.R")
}
source("scel.R")

#' A log function that never returns NaN
#'
#' For x<expansion.point, replaces log(x) with a 2nd order taylor
#' expansion
log.noNaN <- function(x, expansion.point=1e-12) {
  s <- c(log(expansion.point),
         1/expansion.point,
         -0.5/expansion.point^2)
  low <- x<expansion.point
  if (any(low)) {
    y <- log(x)
    y[low] <- sapply(x[low],function(xi)
                     s %*%(xi-expansion.point)^(0:(length(s)-1))  )
  } else {
    y <- log(x)
  }
  return(y)
}

#' Derivative of log.noNaN
dlog.noNaN <- function(x, expansion.point=1e-12) {
  s <- c(1/expansion.point,
       -0.5/expansion.point^2)
  p <- (0:(length(s)-1))
  low <- x<expansion.point
  y <- 1/x
  if (any(low)) {
    y[low] <- sapply(x[low], function(xi)
                     s %*% ((p+1)*(xi-expansion.point)^p)  )
  }
  return(y)
}

library(MASS)

#' Estimate a production function using the approach of Gandhi,
#' Navarro, & Rivers.
#'
#' FIXME: extended description
#'
#' @param production.fn production.fn(param,data) should return log F
#' with parameters param and given data. should also have attribute
#' "gradient" with dlogF/dlogM
#'
#' @param n.param Number of parameters in production.fn
#'
#' @param share.fmla
#' @param step2.instruments
#' @param data
#' @param fixed.effects Not implemented.
#'
#' @param minimize If TRUE, minimize the objective function and return
#' estimates. If FALSE, just returns the objective function
#'
#' @param param0 Initial values for minimization
#'
#' @param conA If not NA, impose constraint that conA %*% param <= conB
#' @param conB If not NA, impose constraint that conA %*% param <= conB
#'
#' @param W Weighiting matrix for GMM. Ignored if empirical.likelihood
#' or continuous.updating are TRUE.
#'
#' @param two.step Whether to estimate in two steps or jointly
#'
#' @param empirical.likelihood Whether to use EL objective function.
#' @param continuous.updating Whether to use CUE objective function.
#'
#' @param least.squares Whether to use a least squares objective
#' function for the first step. Ignored if two.step=FALSE.
#'
#' @param step1.param Vector of logicals of lenth n.param specifying
#' which parameters can be estimated in step1.
#'
#' @param lb Lower bounds on parameters.
#' @param ub Upper bounds on parameters.
#'
#' @param algorithms Vector of two strings specifying optimization
#' algorithms to use in first and second steps. Available options are
#' any nlopt algorithms (see \code{\link[nloptr]{nloptr}}) or 'CMAES'
#' (see \code{\link[cmaes]{cmES}})
#'
#' @param verbosity Amount of information to display. Higher verbosity
#' results in more output
#'
#' @param multi.start Number of initial values to try in the second
#' step. If greater than 1, initial values are drawn from unit
#' simplex.
#'
#' @param time.varying Whether materials coefficient should vary with time.
#'
#' @return S3 object of class 'gnr'
#'
#' @export
production.gnr <- function(
    production.fn, #  production.fn(param,data) should return log F
                   #  with parameters param and given data. should
                   #  also have attribute "gradient" with dlogF/dlogM
    n.param,
    share.fmla,    # share ~ iv1 + iv2
    step2.instruments,  # output ~ iv3 + iv4
    data,
    degree = 1, # degree in omega ~ poly(omega.lag) for step 2
    fixed.effects=FALSE,
    minimize=TRUE,
    param0 = rep(0.3,n.param), # starting point for optimization
    conA=NA, # constrain conA %*% param <= conB
    conB=NA,
    W=NA, # weighting matrix for GMM
    two.step=TRUE,
    empirical.likelihood=FALSE,
    continuous.updating=FALSE,
    least.squares=FALSE,
    time.varying=FALSE, # do coefficients vary with t?
    step1.param=rep(FALSE,n.param), # parameters that can be
                                    # identified in step1
    lb=rep(-1,n.param), # bounds on parameters
    ub=rep(1,n.param),
    algorithms=c("NLOPT_LN_BOBYQA","NLOPT_LN_BOBYQA"),
    verbosity=2,
    penalty=1,
    multi.start=1, # number of initial values to try in second step
    max.eval=1000
    )
{
  # Validate inputs
  if (empirical.likelihood && continuous.updating) {
    stop("Cannot request both empirical.likelihood && continuous.updating.")
  }

  stopifnot(length(param0)==n.param,
            length(lb)==n.param,
            length(step1.param)==n.param,
            length(ub)==n.param)

  if (fixed.effects) {
    stop("fixed.effects not implemented")
  }

  mf <- model.frame(share.fmla,data,na.action=na.pass)
  z1 <- model.matrix(share.fmla,mf)
  s <- model.response(mf,"numeric")

  mf <- model.frame(step2.instruments,data,na.action=na.pass)
  z2 <- model.matrix(step2.instruments,mf)
  y <- model.response(mf,"numeric")

  T <- length(unique(data$t))

  epsilon.fn <- function(param, logF=production.fn(param,data),
                      dlogF=as.vector(attr(logF,"gradient"))) {
    logF <- production.fn(param,data)
    dlogF <- as.vector(attr(logF,"gradient"))
    if (any(dlogF<=0)) {
      warning("dlogF<=0")
    }
    eps <- -s + log.noNaN(dlogF)
    logE <- mean(eps)
    eps <- eps-logE
    return(eps)
  }
  omega.fn <- function(param, eps=epsilon.fn(param), logF=as.vector(production.fn(param,data))) {
    omega <- y - logF - eps
    return(omega)
  }
  eta.fn <- function(param, eps=epsilon.fn(param),
                     omega=omega.fn(param)) {
    omega.lag <- panel.lag(omega,i=data$id,t=data$t)
    dfo <- data.frame(omega=omega,omega.lag=omega.lag)
    lev.reg <- lm(omega ~ poly(omega.lag,degree=degree,raw=TRUE),
                  subset(dfo,!is.na(omega) & !is.na(omega.lag)),
                  na.action=na.exclude)
    eta <- rep(NA,nrow(dfo))
    names(eta) <- rownames(dfo)
    e <- residuals(lev.reg,na.action=na.exclude)
    eta[names(e)] <- e
    return(eta)
  }
  predict <- function(param, logF=production.fn(param,data)) {
    logF <- production.fn(param,data)
    dlogF <- as.vector(attr(logF,"gradient"))
    s.hat <- log.noNaN(dlogF)
    eps <- -s + s.hat
    logE <- mean(eps)
    s.hat <- s.hat + logE
    return(list(output=logF,share=s.hat))
  }

  # objective function
  obj <- function(param, EL=empirical.likelihood,
                  CUE=continuous.updating, step1only=FALSE,
                  step2only=FALSE,
                  LS=least.squares,
                  W=NA, penalty.factor=penalty) {
    ## FIXME: add time dummy
    logF <- production.fn(param,data)
    dlogF <- as.vector(attr(logF,"gradient"))
    small <- 1e-12
    if (any(dlogF<=small)) {
      warning("dlogF<=0")
      return(1e99)
      penalty <- penalty.factor*sum((dlogF[dlogF<=small]-small)^2)
    } else {
      penalty <- 0
    }
    eps <- -s + log.noNaN(dlogF)
    logE <- mean(eps)
    eps <- eps-logE
    if (!step1only) {
      omega <- y - logF - eps
      omega.lag <- panel.lag(omega,i=data$id,t=data$t)
      dfo <- data.frame(omega=omega,omega.lag=omega.lag)
      lev.reg <- lm(omega ~ poly(omega.lag,degree=degree,raw=TRUE),
                    subset(dfo,!is.na(omega) & !is.na(omega.lag)),
                    na.action=na.exclude)
      eta <- rep(NA,nrow(dfo))
      names(eta) <- rownames(dfo)
      e <- residuals(lev.reg,na.action=na.exclude)
      eta[names(e)] <- e
    } else {
      eta <- rep(0,nrow(data))
    }
    if (step2only) {
      eps[] <- 0
      logE <- 0
    }
    if (LS && step1only) {
      return(sum(eps^2) + penalty)
    }
    if (EL) {
      # array of individual moments
      m <- cbind(z1*(eps %*% matrix(1,nrow=1,ncol=ncol(z1))),
                 z2*(eta %*% matrix(1,nrow=1,ncol=ncol(z2))))
      m[is.na(m)] <- 0
      el <- emplik(m,verbose=FALSE)
      #print(sprintf("logelr = %g, sum(log(w)) = %g\n",el$logelr,sum(log(el$wts))))
      .value <- -el$logelr
      return(.value)
    } else { # GMM or CUE
      if (CUE) {
        library(MASS)
        # array of individual moments
        m <- cbind(z1*(eps %*% matrix(1,nrow=1,ncol=ncol(z1))),
                   z2*(eta %*% matrix(1,nrow=1,ncol=ncol(z2))))
        m[is.na(m)] <- 0
        W <- ginv(var(m) +
                  diag(1e-5/nrow(m),nrow=ncol(m),ncol=ncol(m)) )
        M <- colMeans(m)
      } else {
        if (is.na(W)) {
          W <- diag(1,nrow=ncol(z1) + ncol(z2))
        }
        m <- cbind(z1*(eps %*% matrix(1,nrow=1,ncol=ncol(z1))),
                   z2*(eta %*% matrix(1,nrow=1,ncol=ncol(z2))))
        m[is.na(m)] <- 0
        M <- colMeans(m)
        #M <- c(apply(z1,2,function(column)
        #              { sum(column*eps,na.rm=TRUE) }),
        #        apply(z2,2,function(column)
        #              { sum(column*eta,na.rm=TRUE) }))/length(eta)
        #print(cbind(M,M2))
        #print(sum(abs(M-M2)))
      }
      return(M %*% W %*% M + penalty)
    }
  } # end function obj

  library(nloptr)
  multi.obj <- NA
  multi.par <- NA
  if (two.step) {
    np1 <- sum(step1.param==TRUE)
    warning("Assuming dlogF is homogenous of degree 1 in parameters")
    if (np1==1) {
      param <- rep(0,n.param)
      param[step1.param] <- 1
      logF <- production.fn(param,data)
      dlogF <- as.vector(attr(logF,"gradient"))
      step1 <- list()
      step1$param <- 1/mean(exp(-s + log(dlogF)))
      obj1 <- NULL
    } else if (time.varying && np1==T) {
      param <- rep(0,n.param)
      param[step1.param] <- 1
      logF <- production.fn(param,data)
      dlogF <- as.vector(attr(logF,"gradient"))
      step1 <- list()
      step1$param <- aggregate(-s+log(dlogF), list(data$t),
                               function(x) { 1/mean(exp(x))})[,2]
      obj1 <- NULL
      cat("step1$param =",step1$param,"\n")
    } else {
      obj1 <- function(x,Penalty=penalty) {
        p <- rep(0,n.param)
        p[step1.param] <- c(1,x)
        return(obj(p, step1only=TRUE, LS=least.squares,
                   penalty.factor=Penalty))
      }
      if (minimize) {
        x0 <- param0[step1.param][2:np1]
        step1 <- nloptr(x0, obj1,
                        lb=lb[step1.param][2:np1],
                        ub=ub[step1.param][2:np1],
                        opts=list(print_level=verbosity,
                            algorithm=algorithms[1],
                            maxeval=max.eval,
                            xtol_rel=1e-6),Penalty=penalty)
        param <- rep(0,n.param)
        param[step1.param] <- c(1,step1$solution)
        logF <- production.fn(param,data)
        dlogF <- as.vector(attr(logF,"gradient"))
        step1$param <- c(1,step1$solution)*1/mean(exp(-s +
                                                      log(dlogF)))
      } else {
        step1 <- NA
        param <- rep(0,n.param)
        step1$solution <- rep(0,sum(step1.param))
        step1$param <- c(1,step1$solution)
      }
    }
    obj2 <- function(x) {
      p <- rep(0,n.param)
      p[step1.param] <- step1$param
      p[!step1.param] <- x
      return(obj(p, step2only=TRUE))
    }
    p0 <- (1-mean(step1$param))/sum(step1.param==FALSE)
    if (minimize) {
      multi.obj <- rep(NA,multi.start)
      multi.par <- matrix(NA,nrow=multi.start,ncol=sum(step1.param==FALSE) )
      for (r in 1:multi.start) {
        if (r==1) {
          x0 <- param0[step1.param==FALSE]
        } else { # draw starting values uniformly from constant returns
          # to scale simplex
          e <- -log(runif(sum(step1.param==FALSE)))
          x0 <- p0*e/sum(e)
        }
        if (algorithms[2]=="CMAES") {
          require(cmaes)
          step2.trial <- cmaes.custom(x0, obj2,
                                      lower=lb[!step1.param],
                                      upper=ub[!step1.param],
                                      control=list(maxit=max.eval, trace=verbosity>1,
                                          mu=length(param0)*2,
                                          lambda=length(param0)*4,
                                          stop.tolx=1e-6,
                                          logit.bounds=TRUE))
          step2.trial$solution <- step2.trial$par
          step2.trial$objective <- step2.trial$value
        } else {
          step2.trial <- nloptr(x0,obj2 ,
                                lb=lb[!step1.param],
                                ub=ub[!step1.param],
                                opts=list(print_level=verbosity,
                                    algorithm=algorithms[2],
                                    maxeval=max.eval,
                                    xtol_rel=1e-6))
        }
        multi.obj[r] <- step2.trial$objective
        multi.par[r,] <- step2.trial$solution
        if (r==1 || step2.trial$objective<step2$objective) {
          step2 <- step2.trial
        }
      }
      estimates <- rep(NA,n.param)
      estimates[step1.param] <- step1$param
      estimates[!step1.param] <- step2$solution
    } else {
      step2 <- NA
      estimates <- rep(NA,n.param)
    }
  } # end if (two-step)
  else if (minimize) { # joint estimation
    obj2 <- function(x) {
      x1 <- x[1:(sum(step1.param)-1)]
      x2 <- x[(sum(step1.param)):length(x)]
      param[step1.param] <- c(1,x1)
      logF <- production.fn(param,data)
      dlogF <- as.vector(attr(logF,"gradient"))
      p1 <- c(1,x1)*1/mean(exp(-s + log(dlogF)))
      x1 <- x1*p1
      param <- c(x1,x2)
      return(obj(param))
    }
    np1 <- sum(step1.param==TRUE)
    warning("Assuming dlogF is homogenous of degree 1 in parameters")
    x0 <- rep(0,n.param-1)
    step2 <- nloptr(x0,obj2,
                    opts=list(print_level=verbosity,
                        algorithm=algorithms[2],
                        maxeval=1e6,
                        lb=0,
                        ub=1,
                        maxiter=1e6,
                        xtol_rel=1e-6))
    estimates <- step2$solution
    step1 <- NA
  } else {
    step1 <- NA
    step2 <- NA
    estimates <- NA
  }

  result <- list(estimates=estimates,
                 obj=obj,step1=step1,step2=step2,obj1=obj1,obj2=obj2,
                 epsilon=epsilon.fn, omega=omega.fn, predict=predict,
                 z1=z1,z2=z2, multi.obj=multi.obj,
                 multi.par=multi.par,
                 eta=eta.fn)
  class(result) <- "gnr"
  return(result)
}

library(plm)
#' Estimate a production function using a modification of the approach
#' of Gandhi, Navarro, & Rivers.
#'
#' FIXME: extended description
#'
#' @param prod.fmla Formula representing the production function
#'
#' @param share Log share of materials, a character giving the
#' variable name in data
#'
#' @param material Log materials, a character giving the
#' variable name in data
#'
#' @param data Data frame for estimation. Must contain a firm
#' identifier called id and time identifier called t.
#'
#' @param degree Degree of polynomial in lagged inputs to use as proxy
#' for lagged omega
#'
#' @param include.eps If TRUE, estimate using y.tilde = y - bm*m. If
#' FALSE estimate using y.tilde = y - bm*m - eps.
#'
#' @param time.varying Whether materials coefficient should vary with time.
#'
#' @return List consisting of coefficient estimates and plm results.
#'
#' @export
gnr.lm <- function(prod.fmla, share, material, data,
                   degree=1,
                   include.eps=TRUE,
                   time.varying=FALSE)
{
  stopifnot(is.character(share))
  s <- data[,share]
  stopifnot(is.character(material))
  m <- data[,material]

  mf <- model.frame(prod.fmla,data,na.action=na.exclude)
  vars <- attr(terms(prod.fmla),"term.labels")
  y <- model.response(mf,"numeric")

  if (time.varying) {
    step1 <- lm(exp(-s) ~ as.factor(t) -1, data)
    bm <- 1/step1$coef
    eps <- -residuals(lm(s ~ as.factor(t), data),na.action=na.exclude)
    bmt <- 1/predict(step1,newdata=data)
    if (include.eps) {
      y.tilde <- y - bmt*m
    } else {
      y.tilde <- y - bmt*m - eps
    }
  } else {
    eps <- -s+mean(s)
    bm <- 1/mean(exp(-s))
    names(bm) <- material
    if (include.eps) {
      y.tilde <- y - bm*m
    } else {
      y.tilde <- y - bm*m - eps
    }
  }
  df <- data.frame(id=data$id,t=data$t,y.tilde=y.tilde)
  for(v in vars) {
    df[,v] <- data[,v]
  }
  pdf <- pdata.frame(df,index=c("id","t"))
  fmla <- as.formula(
      paste("y.tilde ~",
            paste(vars[vars!=material], collapse=" + "),
            " + poly(",
            paste(sprintf("lag(%s,1)",vars),
                  collapse=" , "),
            ", degree=degree,raw=TRUE)",sep=" ")
      )
  est <- plm(fmla, data=pdf,
             na.action=na.exclude, model="pooling")
  coef <- c(bm,est$coef[vars[vars!=material]])
  names(coef) <- c(names(bm), vars[vars!=material])
  return(list(estimates=coef,plm=est))
}

