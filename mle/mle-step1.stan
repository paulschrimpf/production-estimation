data {
  int<lower=1> NT;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  vector<lower=0>[nmix] alpha;
}


parameters {
  #real rho;
  real<lower=0.0, upper=2.0> bm[nmix];
  #real<lower=0.0, upper=2.0> bk[nmix];
  real<lower=0.0, upper=2.0> bl[nmix];
  cholesky_factor_cov[2] LEps[nmix]; 
  #real<lower=0> sigeta[nmix];
  #real b0[nmix,T];
  simplex[nmix] prob;

  #real rhoK[nmix,2]; 
  #real<lower=0> sigk[nmix];
  #cov_matrix[2] var0[nmix]; # initial variance of omega,k
  #vector[2] mu0[nmix];
}

transformed parameters {
  cov_matrix[2] SigmaEps[nmix];
  for(j in 1:nmix) {
    SigmaEps[j] = LEps[j] * LEps[j]';
  }
}

model {
  real ps[nmix];
  prob ~ dirichlet(alpha);
  #bm ~ normal(0,10);
  #bl ~ normal(0,10);
  for (n in 1:NT) {
    int t;
    int i;
    t = time[n];
    i = id[n];
    if (n==1 || id[n]!=id[n-1]) { # new observation
      for(j in 1:nmix) {
        ps[j] = log(prob[j]);
      }
    }
    for (j in 1:nmix) {
      vector[2] eps;
      eps[1] = log(bm[j]) + 0.5*SigmaEps[j][1,1] - m[n] + y[n];
      eps[2] = l[n] - m[n] - log(bl[j]) + log(bm[j]) + 0.5*SigmaEps[j][2,2];
      //eps[2] = -log(bl[j]) - 0.5*(SigmaEps[j][1,1]-SigmaEps[j][2,2]) +
      //  l[n] - y[n] + eps[1];
      ps[j] = ps[j] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
                                                 LEps[j]);
    }
    if (n==NT || id[n+1]!=id[n]) {
      #print(id[n]);
      #print(ps);
      target += log_sum_exp(ps);
    }
  }
}
