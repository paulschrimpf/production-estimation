data {
  int<lower=1> NT;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  real w[N,nmix];
  int<lower=1,upper=nmix> j;
}


parameters {
  real<lower=0.0, upper=2.0> bm;
  real<lower=0.0, upper=2.0> bl;
  cholesky_factor_cov[2] LEps; 
}

transformed parameters {
  real loglike_i[N];
  matrix[2,2] SigmaEps;
  SigmaEps = LEps * LEps';

  { 
    for (n in 1:NT) {
      int t;
      int i;
      vector[2] eps;

      t = time[n];
      i = id[n];
      if (n==1 || id[n]!=id[n-1]) { # new observation
        loglike_i[i] = 0;
      } else {
        if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
      }
    
      eps[1] = log(bm) + 0.5*SigmaEps[1,1] - m[n] + y[n];
      eps[2] = l[n] - m[n] - log(bl) + log(bm) + 0.5*SigmaEps[2,2];
      loglike_i[i] = loglike_i[i] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
                                                 LEps);
    }
  }
}
model {
  for (n in 1:N) {
    target += loglike_i[n]*w[n,j];
  }
}


  
