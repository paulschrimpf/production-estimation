library(rstan)
load("compiledStanModel.Rdata")
fileName <- "mle-joint.stan"
txt <- readChar(fileName, file.info(fileName)$size)
txt <- gsub("\n$","",txt)
if (!exists("stan.mle") || txt != stan.mle@model_code[1]) {
  stan.mle <- stan_model(file=fileName,
                         model_name="MLE",
                         verbose=TRUE,
                         save_dso=TRUE)
  #stan.fixedp <- stan_model(file="mle-pfixed.stan",
  #                          model_name="MLE",
  #                          verbose=TRUE,
  #                          save_dso=TRUE)
  stan.step1 <- stan_model(file="mle-step1.stan",
                           model_name="MLE Step 1",
                           verbose=FALSE)
  save(stan.mle, stan.step1, stan.fixedp, file="compiledStanModel.Rdata")
}

#' estimate model by maximum likelihood
#'
#'
#' @param types Number of types
#' @param data data.frame
#' @param y Name of output variable in data frame or the output
#'   variable itself. If data is null, must be the output variable
#'   itself.
#' @param m materials (see y for details)
#' @param l labor (see y for details)
#' @param k capital (see y for details)
#' @param id firm identifier (either name or vector, same as y)
#' @param t time identifier (either name or vector, same as y)
#' @param fix.step1 if TRUE, the parameters that can be estimated in
#'   the first step (the share equations) will be held fixed in the
#'   second step. If FALSE, all parameters will be free.
#' @param initial.values
#' @param skip.step1 can only be TRUE if initial values supplied
#' @param algorithm Optimization algorithm. Any algorithm from nloptr
#'   may be used. Algorithms from optimx may be used by preceding the
#'   algorithm name with "OPTIMX_". In addition, "IPOPT" may be specified to use the
#'   ipoptr package. STAN's optimization algorithms can be used by preceding
#'   the algorithm with "STAN_". All parameters must be
#'   free when using the stan algorithms.
#' @param verbose if TRUE, then print extra information (FIXME ---
#'   make this affect output from optimization)
#' @param lo Set initial.value -lo as lower bound on parameters. Some
#'   algorithms will fail without bounds. Does not affect "STAN_" algorithms
#' @param hi Set initial.value + hi as upper bound on parameters.
#'
#' @return
#'
#' @keywords
#' @export
#' @examples
#'
estimate.ml <- function(types=1, data=NULL,  y, m, l,
                        k, id, t,
                        fix.step1=TRUE, initial.values=NULL,
                        skip.step1=FALSE,
                        algorithm="NLOPT_LD_LBFGS",
                        verbose=TRUE, lo=10, hi=10)
{
  if (!is.null(data)) {
    stopifnot(is.character(c(id,t,y,m,l,k)))
    df <- data[,c(id,t,y,m,l,k)]
    names(df) <- c("id","t","y","m","l","k")
  } else {
    df <- data.frame(id=id,t=t,y=y,m=m,l=l,k=k)
  }
  df <- df[order(df$id,df$t),]
  df$t <- df$t-min(df$t)+1
  J <- types
  stan.data <- list(NT=nrow(df), id=df$id, time=df$t,
                    nmix=J, N=length(unique(df$id)),
                    T=length(unique(df$t)),
                    m=df$m, l=df$l,
                    k=df$k, y=df$y,
                    alpha=rep(1,J))
  if (is.null(initial.values)) {
    init <- list(bm=rep(0.3,J), bl=rep(0.3,J),
                 prob=rep(1/J,J),SigmaEps=array(0,dim=c(J,2,2)))
    init$SigmaEps[,1,1] <- 1
    init$SigmaEps[,2,2] <- 1
    init$LEps <- init$SigmaEps*NA
    for (j in 1:J) init$LEps[j,,] <-
                     t(chol(init$SigmaEps[j,,]))
  } else {
    init <- initial.values
  }
  if (skip.step1) {
    opt1 <- list()
    opt1$par <- initial.values
  } else {
    if (verbose) cat("Beginning step 1 optimization\n")
    opt1 <- optimizing(stan.step1, data=stan.data, init=init,
                       algorithm="BFGS",verbose=TRUE,as_vector=FALSE,hessian=FALSE)
    if (verbose) cat("Step 1 optimization complete\n")
  }

  ## Prepare for step 2 optimization
  if (is.null(initial.values)) {
    init <- opt1$par
    init$bk <- 1-init$bl-init$bm
    init$rho <- rep(0.9,J)
    init$sigEta <- rep(1, J)
    init$rhoK <- matrix(0,nrow=J,ncol=3)
    init$sigK <- rep(sd(df$k),J)
    init$muk <- rep(mean(df$k),J)
    init$var0 <- array(0,dim=c(J,2,2))
    init$var0[,1,1] <- 1
    init$var0[,2,2] <- 1
    init$L0 <- array(0,dim=c(J,2,2))
    for (j in 1:J) init$L0[j,,] <- t(chol(init$var0[j,,]))
    init$b0 <-t(sapply(1:J,function(j)
      aggregate(df$y - init$bm[j]*df$m -
                init$bl[j]*df$l
                ~ df$t,
                FUN=mean)[,2]))
    print(init$b0)
  } else {
    init <- initial.values
    for(n in names(opt1$par)) {
      init[[n]] <- opt1$par[[n]]
    }
  }

  ## Create objective function and score
  mle.aux <- sampling(stan.mle, data=stan.data,
                      chains=0)
  loglikelihood <- function(parm) {
    if (is.list(parm)) {
      x <- unconstrain_pars(mle.aux, parm)
    } else {
      x <- parm
    }
    ll <- log_prob(mle.aux,x,adjust_transform=FALSE)
    ll <- ifelse(is.nan(ll),-Inf,ll)
    return(ll)
  }
  score <- function(parm) {
    if (is.list(parm)) {
      x <- unconstrain_pars(mle.aux, parm)
    } else {
      x <- parm
    }
    ll <- grad_log_prob(mle.aux,x)
    return(ll)
  }

  ## fix some parameters if requested
  x0 <- unconstrain_pars(mle.aux,init)
  if (fix.step1) {
    foo <- init
    foo$prob <- rep(1/J,J)
    foo$bl <- 0*foo$bl
    foo$bm <- 0*foo$bm
    foo$LEps <- 10*foo$LEps
    xhi <- unconstrain_pars(mle.aux,foo)
    #
    cx <- xhi!=x0
    xunc <- function(x) {
      xall <- rep(NA,length(cx))
      xall[cx] <- x0[cx]
      xall[!cx] <- x
      return(xall)
    }
    xcon <- function(x) {
      xcon <- x[!cx]
    }
  } else {
    cx <- rep(FALSE,length(x0))
    xunc <- function(x) x
    xcon <- function(x) x
  }


  if (grepl("NLOPT", algorithm)) {
    library(nloptr)
    opt2 <- nloptr(xcon(x0), function(x) -loglikelihood(xunc(x)),
                   function(x) {
                     gall <- -score(xunc(x))
                     gall[!cx]
                   },
                   lb = xcon(x0)-lo, ub = xcon(x0)+hi,
                   opts=list(print_level=3, algorithm=algorithm,
                             maxeval=10000))
    est2 <- constrain_pars(mle.aux, xunc(opt2$solution))
    loglike <- -opt2$objective
  } else if (grepl("OPTIMX", algorithm)) {
    library(optimx)
    algo <- gsub("OPTIMX_","",algorithm)
    opt2 <- optimx(xcon(x0),
                   fn=function(x) loglikelihood(xunc(x)),
                   gr=function(x) {
                     gall <- score(xunc(x))
                     gall[!cx]
                   },
                   lower = xcon(x0)-lo, upper = xcon(x0)+hi,
                   method=algo,
                   itnmax=10000,
                   control=list(trace=4, maximize=TRUE,
                                starttests=FALSE, REPORT=1))
    est2 <- constrain_pars(mle.aux, xunc(coef(opt2)))
    loglike <- opt2["value"]
  } else if (algorithm=="IPOPT") {
    library(ipoptr)
    opt2 <- ipoptr(xcon(x0), eval_f = function(x) -loglikelihood(xunc(x)),
                   eval_grad_f = function(x) {
                     gall <- score(xunc(x))
                     gall[!cx]
                   },
                   lb = xcon(x0)-lo, ub = xcon(x0)+hi,
                   opts=list(print_level=5,tol=1e-6))
    est2 <- constrain_pars(mle.aux, opt2$solution)
  } else if (grepl("STAN_",algorithm)) {
    if (fix.step1)
      warning("STAN algorithms only allow all parameters to be free in second step.")
    algo <- gsub("STAN_","",algorithm)
    opt2 <-   optimizing(stan.mle, data=stan.data, init=init,
                         algorithm=algo,verbose=TRUE,as_vector=FALSE,hessian=FALSE,
                         tol_grad = 1e-8, tol_rel_grad = 1e-8)
    est2 <- opt2$par
    loglike <- opt2$value
  }

  return(list(par=est2, opt1=opt1, opt2=opt2, loglike=loglike))

}

