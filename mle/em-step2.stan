data {
  int<lower=1> NT;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  real w[N,nmix];
  int<lower=1,upper=nmix> j;

  real<lower=0.0, upper=2.0> bm;
  real<lower=0.0, upper=2.0> bl;
  cholesky_factor_cov[2] LEps;  
}

transformed data {
  
  real ll1[NT];
  real eps1[NT];
  matrix[2,2] SigmaEps;
  
  SigmaEps = LEps * LEps';
  for (n in 1:NT) {
    int t;
    int i;
    vector[2] eps;
    
    eps[1] = log(bm) + 0.5*SigmaEps[1,1] - m[n] + y[n];
    eps[2] = l[n] - m[n] - log(bl) + log(bm) + 0.5*SigmaEps[2,2];
    ll1[n] = multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
                                        LEps);
    eps1[n] = eps[1];
  }
}

parameters {
  real rho;
  real<lower=0.0, upper=2.0> bk;
  real<lower=0> sigEta;
  real b0[T];
  
  real rhoK[3]; 
  real<lower=0> sigK;
  cholesky_factor_cov[2] L0;
  real muk;
}

transformed parameters {
  real loglike_i[N]; 
  
  { 
    real omega[T];
    for (n in 1:NT) {
      int t;
      int i;
      vector[2] eps;

      t = time[n];
      i = id[n];
      if (n==1 || id[n]!=id[n-1]) { # new observation
        loglike_i[i] = 0;
      } else {
        if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.");
      }
    
      loglike_i[i] = loglike_i[i] + ll1[n];
      omega[t] = y[n] - bm*m[n] - bl*l[n] - bk*k[n] -
        eps1[n] - b0[t];
      loglike_i[i] = loglike_i[i] + log(fabs(1 - bm - bl));
      if (n==1 || id[n] != id[n-1]) { # new observation
        vector[2] x;
        vector[2] mu;
        x[1] = omega[t];
        x[2] = k[n];
        mu[1] = 0;
        mu[2] = muk;
        loglike_i[i] = loglike_i[i] + multi_normal_cholesky_lpdf(x | 
                                                   mu, L0);
      } else {
         loglike_i[i] = loglike_i[i] + normal_lpdf(omega[t] | rho*omega[t-1],
                                     sigEta);
         loglike_i[i] = loglike_i[i] + normal_lpdf(k[n] |  rhoK[1] + rhoK[2]*k[n-1] +
                                     rhoK[3]*omega[t-1], sigK);
      }
    }
  }
}
model {
  for (n in 1:N) {
    target += loglike_i[n]*w[n,j];
  }
}

generated quantities {
  matrix[2,2] var0; # initial variance of omega,k
  var0 = L0 * L0';
}
  
