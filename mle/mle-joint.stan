data {
  int<lower=1> NT;
  int<lower=1> T;  
  int<lower=1> nmix;
  int<lower=1> N;
  int id[NT];
  int time[NT];
  real m[NT];
  real k[NT];
  real l[NT];
  real y[NT];
  vector<lower=0>[nmix] alpha;
}


parameters {
  real rho[nmix];
  real<lower=0.0, upper=2.0> bm[nmix];
  real<lower=0.0, upper=2.0> bk[nmix];
  real<lower=0.0, upper=2.0> bl[nmix];
  real<lower=0> sigEta[nmix];
  real b0[nmix,T];
  simplex[nmix] prob;
  cholesky_factor_cov[2] LEps[nmix]; 
  
  real rhoK[nmix,3]; 
  real<lower=0> sigK[nmix];
  cholesky_factor_cov[2] L0[nmix];
  real muk[nmix];
}

transformed parameters {
  matrix[2,2] SigmaEps[nmix];
  matrix[2,2] var0[nmix]; # initial variance of omega,k
  for(j in 1:nmix) {
    SigmaEps[j] = LEps[j] * LEps[j]';
    var0[j] = L0[j] * L0[j]';
  }
}
model {
  real ps[nmix];
  #bm ~ normal(0,10);
  #bl ~ normal(0,10);
  real omega[nmix,T];

  # prior for prob
  prob ~ dirichlet(alpha);
  for (n in 1:NT) {
    int t;
    int i;
    t = time[n];
    i = id[n];
    if (n==1 || id[n]!=id[n-1]) { # new observation
      for(j in 1:nmix) {
        ps[j] = log(prob[j]);
      }
    } else {
      if (time[n-1] != t-1) print("ERROR: data not sorted or panel unbalanced.")
    }
    
    for (j in 1:nmix) {      
      vector[2] eps;
      eps[1] = log(bm[j]) + 0.5*SigmaEps[j][1,1] - m[n] + y[n];
      eps[2] = l[n] - m[n] - log(bl[j]) + log(bm[j]) + 0.5*SigmaEps[j][2,2];
      ps[j] = ps[j] + multi_normal_cholesky_lpdf(eps | rep_vector(0,2),
                                                 LEps[j]);
      omega[j,t] = y[n] - bm[j]*m[n] - bl[j]*l[n] - bk[j]*k[n] -
        eps[1] - b0[j,t];
      ps[j] = ps[j] + log(fabs(1 - bm[j] - bl[j]));
      if (n==1 || id[n] != id[n-1]) { # new observation
        vector[2] x;
        vector[2] mu;
        x[1] = omega[j,t];
        x[2] = k[n];
        mu[1] = 0;
        mu[2] = muk[j];
        ps[j] = ps[j] + multi_normal_cholesky_lpdf(x | 
                                                   mu, L0[j]);
      } else {
         ps[j] = ps[j] + normal_lpdf(omega[j,t] | rho[j]*omega[j,t-1],
                                     sigEta[j]);
         ps[j] = ps[j] + normal_lpdf(k[n] |  rhoK[j,1] + rhoK[j,2]*k[n-1] +
                                     rhoK[j,3]*omega[j,t-1], sigK[j]);
      }
    }
    if (n==NT || id[n+1]!=id[n]) {
      #print(id[n]);
      #print(ps;)
      target += log_sum_exp(ps);
    }
  }
}
